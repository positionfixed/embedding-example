# README #

This example demonstrates how you can integrate the BrowseEmAll Core API into a .Net Application. 

The Core API is necessary to run the example, get it at [BrowseEmAll](https://www.browseemall.com/CoreAPI).
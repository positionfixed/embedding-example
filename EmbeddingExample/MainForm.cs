﻿using BrowseEmAll.API;
using BrowseEmAll.API.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmbeddingExample
{
    public partial class MainForm : Form
    {
        private BrowserManager manager = new BrowserManager(IntegrationMode.Winforms);
        private BrowserSettings settings = new BrowserSettings();

        private int browserId;

        public MainForm()
        {
            InitializeComponent();

            // Register different events
            manager.OnBrowserReady += Manager_OnBrowserReady;
            manager.OnNavigateComplete += Manager_OnNavigateComplete;
            manager.OnTabSelected += Manager_OnTabSelected;
            manager.OnJavaScriptExecuted += Manager_OnJavaScriptExecuted;
            manager.OnScreenshotCompleted += Manager_OnScreenshotCompleted;

            // Load Browser Instance (you could change the rendering engine to 'FIREFOX44' or 'IE11' or 'APPLE_IPHONE_6 for example)
            Control browser = (Control)manager.OpenBrowser(Browser.CHROME48, settings);

            browser.Dock = DockStyle.Fill;
            panelBrowser.Controls.Add(browser);
        }

        #region Events

        // A screenshot was taken
        private void Manager_OnScreenshotCompleted(object sender, BrowseEmAll.API.Events.ScreenshotCompletedEventArgs e)
        {
            // Show screenshot in form
            Bitmap bmp;
            using (var ms = new MemoryStream(e.Screenshot))
            {
                bmp = new Bitmap(ms);
            }

            ScreenshotForm form = new ScreenshotForm(bmp);
            form.ShowDialog();
            bmp.Dispose();
        }

        // JavaScript was executed successfully
        private void Manager_OnJavaScriptExecuted(object sender, BrowseEmAll.API.Events.JavaScriptExecuteEventArgs e)
        {
            if (e.Type == BrowseEmAll.API.Events.ReturnType.Nothing)
            {
                MessageBox.Show("The JavaScript did not return any value!");
            }
            else
            {
                MessageBox.Show("Your JavaScript returned: " + e.Value);
            }
        }

        // The user selected a different tab
        private void Manager_OnTabSelected(object sender, BrowseEmAll.API.Events.TabEventArgs e)
        {
            txtUrl.Text = e.Url;
        }

        // The browser has navigated to another page
        private void Manager_OnNavigateComplete(object sender, BrowseEmAll.API.Events.NavigationCompleteEventArgs e)
        {
            txtUrl.Text = e.Url;
        }

        #endregion

        #region Actions

        private void Manager_OnBrowserReady(object sender, BrowseEmAll.API.Events.BrowserReadyEventArgs e)
        {
            browserId = e.BrowserID;
            manager.Navigate(e.BrowserID, txtUrl.Text);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            manager.Back(browserId);
        }

        private void btnForward_Click(object sender, EventArgs e)
        {
            manager.Forward(browserId);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            manager.Refresh(browserId);
        }

        private void btnScreenshot_Click(object sender, EventArgs e)
        {
            manager.GetScreenshot(browserId);
        }

        private void btnJavaScript_Click(object sender, EventArgs e)
        {
            manager.ExecuteJavaScript(browserId, txtJavaScript.Text);
        }

        private void txtUrl_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            // if enter was pressed try navigating to the new url
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (!(txtUrl.Text.StartsWith("http://") || txtUrl.Text.StartsWith("https://")))
                {
                    txtUrl.Text = "http://" + txtUrl.Text;
                }

                manager.Navigate(browserId, txtUrl.Text);
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            manager.CloseBrowser(browserId);
        }

        #endregion


    }
}

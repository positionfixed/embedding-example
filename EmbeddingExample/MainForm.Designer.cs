﻿namespace EmbeddingExample
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panelMenu = new System.Windows.Forms.Panel();
            this.txtUrl = new System.Windows.Forms.TextBox();
            this.btnScreenshot = new System.Windows.Forms.Button();
            this.txtJavaScript = new System.Windows.Forms.TextBox();
            this.btnJavaScript = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnForward = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.panelBrowser = new System.Windows.Forms.Panel();
            this.panelMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMenu
            // 
            this.panelMenu.Controls.Add(this.txtUrl);
            this.panelMenu.Controls.Add(this.btnScreenshot);
            this.panelMenu.Controls.Add(this.txtJavaScript);
            this.panelMenu.Controls.Add(this.btnJavaScript);
            this.panelMenu.Controls.Add(this.btnRefresh);
            this.panelMenu.Controls.Add(this.btnForward);
            this.panelMenu.Controls.Add(this.btnBack);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMenu.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(1832, 40);
            this.panelMenu.TabIndex = 0;
            // 
            // txtUrl
            // 
            this.txtUrl.AcceptsReturn = true;
            this.txtUrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUrl.Location = new System.Drawing.Point(509, 0);
            this.txtUrl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.Size = new System.Drawing.Size(719, 35);
            this.txtUrl.TabIndex = 9;
            this.txtUrl.Text = "http://www.google.de";
            this.txtUrl.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUrl_KeyPress_1);
            // 
            // btnScreenshot
            // 
            this.btnScreenshot.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnScreenshot.Location = new System.Drawing.Point(1228, 0);
            this.btnScreenshot.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnScreenshot.Name = "btnScreenshot";
            this.btnScreenshot.Size = new System.Drawing.Size(238, 40);
            this.btnScreenshot.TabIndex = 8;
            this.btnScreenshot.Text = "Screenshot";
            this.btnScreenshot.UseVisualStyleBackColor = true;
            this.btnScreenshot.Click += new System.EventHandler(this.btnScreenshot_Click);
            // 
            // txtJavaScript
            // 
            this.txtJavaScript.Dock = System.Windows.Forms.DockStyle.Right;
            this.txtJavaScript.Location = new System.Drawing.Point(1466, 0);
            this.txtJavaScript.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtJavaScript.Multiline = true;
            this.txtJavaScript.Name = "txtJavaScript";
            this.txtJavaScript.Size = new System.Drawing.Size(148, 40);
            this.txtJavaScript.TabIndex = 7;
            this.txtJavaScript.Text = "return 7+4;";
            // 
            // btnJavaScript
            // 
            this.btnJavaScript.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnJavaScript.Location = new System.Drawing.Point(1614, 0);
            this.btnJavaScript.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnJavaScript.Name = "btnJavaScript";
            this.btnJavaScript.Size = new System.Drawing.Size(218, 40);
            this.btnJavaScript.TabIndex = 6;
            this.btnJavaScript.Text = "ExecuteJS";
            this.btnJavaScript.UseVisualStyleBackColor = true;
            this.btnJavaScript.Click += new System.EventHandler(this.btnJavaScript_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnRefresh.Location = new System.Drawing.Point(320, 0);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(189, 40);
            this.btnRefresh.TabIndex = 3;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnForward
            // 
            this.btnForward.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnForward.Location = new System.Drawing.Point(158, 0);
            this.btnForward.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnForward.Name = "btnForward";
            this.btnForward.Size = new System.Drawing.Size(162, 40);
            this.btnForward.TabIndex = 2;
            this.btnForward.Text = "Forward";
            this.btnForward.UseVisualStyleBackColor = true;
            this.btnForward.Click += new System.EventHandler(this.btnForward_Click);
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBack.Location = new System.Drawing.Point(0, 0);
            this.btnBack.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(158, 40);
            this.btnBack.TabIndex = 1;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // panelBrowser
            // 
            this.panelBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBrowser.Location = new System.Drawing.Point(0, 40);
            this.panelBrowser.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panelBrowser.Name = "panelBrowser";
            this.panelBrowser.Size = new System.Drawing.Size(1832, 1571);
            this.panelBrowser.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1832, 1611);
            this.Controls.Add(this.panelBrowser);
            this.Controls.Add(this.panelMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MainForm";
            this.Text = "BrowseEmAll API Embedding Example";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.panelMenu.ResumeLayout(false);
            this.panelMenu.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Button btnScreenshot;
        private System.Windows.Forms.TextBox txtJavaScript;
        private System.Windows.Forms.Button btnJavaScript;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnForward;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Panel panelBrowser;
        private System.Windows.Forms.TextBox txtUrl;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmbeddingExample
{
    public partial class ScreenshotForm : Form
    {
        public ScreenshotForm(Bitmap image)
        {
            InitializeComponent();

            picScreenshot.Image = image;
        }
    }
}
